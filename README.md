# Runwild 

Does binary search really take less steps to find an item? Does linear search performe better in best case scenarios? Let your algorithms run wild and find out. 

I wrote this little script as I was first learning about differencies in search algorithms to help me understand and relate more to the topic.

This **command-line program** lets you:

*  feed different search algorithms with inputs of your choice 
*  choose among different scenarios (best case, worst case)
*  compare the algorithm performance

## Tools

- Python 3.7

## TODOs and beyond

- implement an interactive visualization of the algorithms behavior

## Screenshot

Example output:

![output_table](img/table.jpg)

## Usage

```
$ git clone https://gitlab.com/moryama/runwild.git`
$ cd runwild
$ python3 run.py
```

## License

No license is available at the moment.