'''This module contains the functions that get user input.'''


def get_algs_choice(available_algs):
    
    print('=' * 20)
    print('CHOOSE WHICH ALGORITHM(S) TO RUN:')

    for alg in available_algs:
        print(alg.idn + '. ' + alg.name)

    print('Type ENTER when you\'re done.')

    alg_idns = [alg.idn for alg in available_algs]
    alg_to_run = []
    
    while True:

        if len(alg_to_run) == len(alg_idns):
            break
        
        answer = input('Type a number: ')

        if not answer:
            break
        elif answer not in alg_idns:
            print('Please type a valid number.')
        elif answer in alg_to_run:
            print('You already selected that.')
        else:
            alg_to_run.append(answer)
    
    print('=' * 20)
    return alg_to_run


def get_input_size_choice():

    print('INPUT SIZE:')
    print('(You can type in more than one.)')
    print('Type ENTER when you\'re done.')
    
    input_to_run = []
    while True:
        answer = input('Type a number: ')
        if not answer:
            break
        try:
            answer = int(answer)    
            input_to_run.append(answer)
        except ValueError:
            print('Doesn\'t look like a number.')
            continue
    
    print('=' * 20)
    return input_to_run


def get_scenario_choice(available_scenarios):

    print('AVAILABLE SCENARIOS:')
    
    for scenario in available_scenarios:
        print(scenario.idn + '. ' + scenario.name)
    
    print('(You can choose in more than one.)')
    print('Type ENTER when you\'re done.')

    scenario_idns = [scenario.idn for scenario in available_scenarios]
    scenario_to_run = []
    
    while True:

        if len(scenario_to_run) == len(scenario_idns):
            break
        answer = input('Type a number: ')    
        
        if not answer:
            break
        elif answer not in scenario_idns:
            print('Please type a valid number.')
        elif answer in scenario_to_run:
            print('You already selected that.')
        else:
            scenario_to_run.append(answer)

    print('=' * 20)
    return scenario_to_run