def binary_search(arr, item):
    '''Binary Search'''
    
    steps = 0
    
    if len(arr) != 0:

        # work on a copy of arr top keep the original arr intact for any further running of the alg 
        arr = arr[:]
        
        while len(arr) != 0:
            
            steps += 1
            mid_index = (len(arr) // 2)
            mid = arr[mid_index]
            
            if item == mid:
                return steps
            elif item > mid:
                del arr[:mid_index + 1]
            elif item < mid:
                del arr[mid_index:]     
    steps += 1

    return steps