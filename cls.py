'''This module contains the definition of classes used in the program.'''


from time import perf_counter

class Algorithm():

    def __init__(self, name, oog, func, idn, filepath):
        self.name = name
        self.oog = oog
        self.func = func
        self.idn = idn
        self.filepath = filepath
    
    def run(self, arr, target):
        '''Run the algorithm and log its execution time.'''
        
        start = perf_counter()
        self.steps = self.func(arr, target)
        stop = perf_counter()
        self.exec_time = stop - start
        
        return True

    def print_script(self):
        '''Print the algorithm code.'''

        with open(self.filepath) as f:
            self.script = f.read()
        
        return self.script


class Scenario():

    def __init__(self, name, idn):
        self.name = name
        self.idn = idn
    
    def set_target(self, arr):
        '''Depending on the scenario, set the index of the target item in a given array.'''

        if self.name == 'best case':
            self.target = arr[0]
        elif self.name == 'middle':
            self.target = arr[len(arr) // 2]
        elif self.name == 'worst case':
            self.target = arr[-1]

        return self.target
