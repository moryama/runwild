import unittest
from helpers import zip_sort_arr


class TestZipSortFunction(unittest.TestCase):

    def test_basic(self):
        arr = ['a', 'a', 'a', 'b', 'b', 'b']
        t = 2
        e = 3
        output = ['a', 'b', 'a', 'b', 'a', 'b']
        self.assertEqual(zip_sort_arr(arr, t, e), output)

    def test_two_elements(self):
        arr = ['a', 'b']
        t = 2
        e = 1
        output = ['a', 'b']
        self.assertEqual(zip_sort_arr(arr, t, e), output)

    def test_empty_array(self):
        arr = [0]
        t = 0
        e = 0
        output = []
        self.assertEqual(zip_sort_arr(arr, t, e), output)

    def test_same_t_and_e(self):
        arr = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c']
        t = 3
        e = 3
        output = ['a', 'b', 'c', 'a', 'b', 'c', 'a', 'b', 'c']
        self.assertEqual(zip_sort_arr(arr, t, e), output)

    def test_nested_lists(self):
        arr = [['a'], ['a'], ['b'], ['b'], ['c'], ['c']]
        t = 3
        e = 2
        output = [['a'], ['b'], ['c'], ['a'], ['b'], ['c']]
        self.assertEqual(zip_sort_arr(arr, t, e), output)
    
    def test_nested_lists_two_elements(self):
        arr = [['a'], ['b']]
        t = 2
        e = 1
        output = [['a'], ['b']]
        self.assertEqual(zip_sort_arr(arr, t, e), output)

    