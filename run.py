'''From here the program is run.''' 

import ui
import inst
from helpers import generate_num_list
from helpers import generate_table
from helpers import zip_sort_arr


def runwild():

    # Collect the variables to run: 
    
    # Algorithms
    available_algs = inst.get_available_algs()
    chosen_algs = ui.get_algs_choice(available_algs)
    algs_to_run = [alg for alg in available_algs if alg.idn in chosen_algs]
    # Inputs of a certain size
    input_sizes = ui.get_input_size_choice()
    inputs_to_run = [generate_num_list(input_size) for input_size in input_sizes]
    # Indexes of the targets
    available_scenarios = inst.get_available_scenarios()
    chosen_scenarios = ui.get_scenario_choice(available_scenarios)
    scenarios_to_run = [scen for scen in available_scenarios if scen.idn in chosen_scenarios]

    # Run all possible combinations
    performance_data = []
    for alg in algs_to_run:
        for inpt in inputs_to_run:
            for scenario in scenarios_to_run:
                target = scenario.set_target(inpt)
                len_of_inpt = len(inpt)
                alg.run(inpt, target)
                case_data = [alg.name, alg.oog, len_of_inpt, target, alg.steps, alg.exec_time]
                performance_data.append(case_data)
    
    # Output data in a table
    parameters = ['TYPE OF ALGORITHM', 
                  'ORDER OF GROWTH', 
                  'SIZE OF INPUT', 
                  'TARGET', 
                  'N° OF STEPS', 
                  'EXECUTION TIME']
    sorted_performance_data = zip_sort_arr(performance_data, 
                                           len(algs_to_run), 
                                           (len(inputs_to_run) * len(scenarios_to_run)))
    print(sorted_performance_data)
    
    generate_table(parameters, sorted_performance_data)

    return True

runwild()
