'''Here classes objects are instantiated.'''

import cls

from formulas.linear import linear_search
from formulas.binary import binary_search


def get_available_algs():
    '''Instantiate the algorithms and return a tuple'''

    linear = cls.Algorithm('linear search', 
                       'O(n)', 
                       linear_search, 
                       '1',
                       'formulas/linear.py'
                       )

    binary = cls.Algorithm('binary search', 
                       'log(n)', 
                       binary_search, 
                       '2',
                       'formulas/binary.py'
                       )

    available_algs = (linear, binary)

    return available_algs


def get_available_scenarios():
    '''Instantiate the scenarios and return a tuple'''

    best_case = cls.Scenario('best case', 
                         '1')

    average = cls.Scenario('middle', 
                       '2')
    
    worst_case = cls.Scenario('worst case', 
                          '3')

    available_scenarios = (best_case, average, worst_case)

    return available_scenarios