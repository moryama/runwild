'''This module contains helper functions used within the program.'''

import collections


def generate_num_list(size):
    nums = []
    for i in range(1, size + 1):
        nums.append(i)
    return nums


def generate_table(cols, rows):
    '''Check the bottom of this doc to see what the table will look like.'''

    # Table elements
    width_of_table = sum(len(col) + 2 for col in cols) + len(cols) + 8
    blank_line = []
    titles = []
    data_rows = []

    # Populate elements
    line = '-' * width_of_table

    for col in cols:
        blank_line.append('|' + (' ' * (len(col) + 2)))
        titles.append('|' + ' ' + col + ' ')

    for row in rows:
        data_row = []
        for info, title in zip(row, titles):
            piece_of_data = '|' + ' ' + str(info)
            # To each piece of data we add enough space to cover the length of its title
            data_row.append(piece_of_data + ' ' * (len(title) - len(piece_of_data)))
        data_rows.append(data_row)

    # Print elements
    print(line)    
    print(''.join(blank_line))
    print(''.join(titles))
    print(''.join(blank_line))
    print(line)
    for row in data_rows:
        print(''.join(row)) 
        print(line)

    return True


def zip_sort_arr(arr, t, e):
    '''Takes an array with :t: = len(set(arr))
                           :e: = arr.count(of each element)
    and returns the array zip_sorted. For example:

    arr = ['a', 'a', 'a', 'b', 'b', 'b']
    t, e = 2, 3
    :return: ['a', 'b', 'a', 'b', 'a', 'b']
    ''' 

    zip_sorted_arr = []

    for i in range(e):
        cursor = i
        for j in range(t):
            zip_sorted_arr.append(arr[cursor])
            cursor += e
    
    return zip_sorted_arr


# This is an example of a table returned by generate_table()
# ------------------------------------------------------------------------------------------------------
# |                   |                 |               |        |             |                |
# | TYPE OF ALGORITHM | ORDER OF GROWTH | SIZE OF INPUT | TARGET | N° OF STEPS | EXECUTION TIME |
# |                   |                 |               |        |             |                |
# ------------------------------------------------------------------------------------------------------
# | linear search     | O(n)            | 100           | 1      | 1           | 1.3090000000381963e-05|
# ------------------------------------------------------------------------------------------------------
# | linear search     | O(n)            | 100           | 51     | 51          | 2.149899999981386e-05|
# ------------------------------------------------------------------------------------------------------
# | binary search     | log(n)          | 100           | 1      | 7           | 5.6649000000241756e-05|
# ------------------------------------------------------------------------------------------------------
# | binary search     | log(n)          | 100           | 51     | 1           | 1.0370000000037294e-05|
# ------------------------------------------------------------------------------------------------------
